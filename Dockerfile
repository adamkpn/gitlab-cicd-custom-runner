FROM docker:dind
RUN echo 'http://dl-cdn.alpinelinux.org/alpine/latest-stable/main/' >> /etc/apk/repositories && \
    apk --no-cache update
# Install pip, poetry and tools
RUN apk add --no-cache python3=3.11.6-r1 --repository=http://dl-cdn.alpinelinux.org/alpine/edge/main
RUN apk add --no-cache py3-pip=23.1.2-r0 py3-pylint poetry --repository=http://dl-cdn.alpinelinux.org/alpine/edge/main
